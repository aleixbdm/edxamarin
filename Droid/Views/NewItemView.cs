﻿using Android.OS;
using Android.Support.V7.Widget;
using Android.Views;
using edxamarin.ViewModels;
using Android.App;
using MvvmCross.Droid.Support.V7.AppCompat;
using Android.Support.Design.Widget;
using System;

namespace edxamarin.Droid.Views
{
    [Activity]
    public class NewItemView : MvxAppCompatActivity<NewItemViewModel>
    {
        FloatingActionButton saveButton;
        Android.Widget.EditText title, description;

        protected override void OnCreate(Bundle bundle)
        {

            base.OnCreate(bundle);

            SetContentView(Resource.Layout.NewItemView);

            this.Title = ViewModel.Title;

            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            var actionBar = SupportActionBar;
            if (actionBar != null)
            {
                actionBar.SetDisplayHomeAsUpEnabled(true);
            }

            saveButton = FindViewById<FloatingActionButton>(Resource.Id.save_button);
            title = FindViewById<Android.Widget.EditText>(Resource.Id.txtTitle);
            description = FindViewById<Android.Widget.EditText>(Resource.Id.txtDesc);

            saveButton.Click += SaveButton_Click;
        }

        void SaveButton_Click(object sender, EventArgs e)
        {
            var item = new Item
            {
                Text = title.Text,
                Description = description.Text
            };
            ViewModel.AddItemCommand.Execute(item);

            Finish();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            if (item.ItemId == Android.Resource.Id.Home)
            {
                OnBackPressed();
            }

            return base.OnOptionsItemSelected(item);
        }

    }
}
