﻿using Android.Content;
using MvvmCross.Droid.Platform;
using MvvmCross.Core.ViewModels;
using System.Collections.Generic;
using System.Reflection;
using MvvmCross.Droid.Support.V7.RecyclerView;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;

namespace edxamarin.Droid
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }

        protected override IEnumerable<Assembly> AndroidViewAssemblies => new List<Assembly>(base.AndroidViewAssemblies)
        {
            typeof(MvxRecyclerView).Assembly,
            typeof(Toolbar).Assembly,
            typeof(NavigationView).Assembly
        };
    }
}
