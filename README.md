# Ejercicio de MvvmCross & Realm con Xamarin para Android/iOS

Aplicación desarrollada en Xamarin usando mvvmcross 5.6.3 y realm 2.1.0. Funciona tanto para Android como iOS. Testeada con un Nexus 6 api 25 y emulador nexus 5X api 25 en la app Android, y con un simulador iphone SE 11.2 para la app iOS.

Se ha escogido la temática de dragones, la aplicación consiste en una lista de dragones, podemos consultar su nombre y descripción, y podemos añadir nuevos dragones que seran guardados en la base de datos del teléfono.

Core: el nucleo común de la app contiene lo siguiente:
    - Un App MvxApplication para inizializar los servicios y el view model inicial, una classe con los colores, y un AppStart para inizializar la applicación con Mvvmcross y su navegación.
    - Los view models de la app, uno por cada pantalla y uno de base.
    - Un servicio para obtener y guardar los dragones. Se ha usado una colección IRealmCollection como estructura de datos.
    - El modelo del dragón del tipo RealmObject.

Droid: para la versión Android:
	- Se ha añadido una classe Setup que hereda de MvxAndroidSetup para inicializar la aplicación con la library MvvmCross.
	- Las Views, una para cada pantalla. Heredan de la classe MvxAppCompatActivity y utilizan IMvxCommand para realizar la navegación entre pantallas. Añadir que la pantalla inicial con la lista de dragones utiliza MvxRecyclerView.
	- Las vistas de cada view en la carpeta Resources/layout. El binding de cada componente con su View se ha realizado con local:MvxBind. Y para el caso de la lista de dragones, para el template se ha usado local:MvxItemTemplate.

iOS: para la versión iOS:
	- También se ha añadido una classe Setup que hereda de MvxIosSetup para inicializar la aplicación con la library MvvmCross. Además, el AppDelegate hereda de MvxApplicationDelegate para la inicialización de la vista.
	- Las Views. En este caso, se han creado view controllers de iOS los cuales se generaban una classe .cs para el view controller y un .xib para la interface. 
	Los view controllers heredan de MvxViewController, a excepción de la pantalla de lista de dragones que directamente hereda de MvxTableViewController. También utilizan IMvxCommand para realizar la navegación entre pantallas.
	Las interfaces se han creado con el Xcode Interface Builder y se ha realizado el binding por código en el view controller con CreateBindingSet.
	- Las Cells han sido creadas a parte para ser añadidas en el MvxTableViewController usando un MvxTableViewSource.