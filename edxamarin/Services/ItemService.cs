﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Realms;

namespace edxamarin.Services
{
    public class ItemsService : IItemsService<Item>
    {
        Realm _realm;
        IRealmCollection<Item> _items;

        public ItemsService()
        {
            _realm = Realm.GetInstance();
            _items = (IRealmCollection<Item>)_realm.All<Item>();

            if (_items.Count() == 0) {
                var items = new List<Item>
                {
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Frikin, Protector Of Creatures",
                        Description="Fierce cobalt eyes sit lightly within the creature's horned, narrow skull, which gives the creature a very ominous looking appearance."},
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Ondryt, Firebreath",
                        Description="Wide blazing eyes sit delicately within the creature's soft, scaled skull, which gives the creature a rather docile looking appearance."},
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Esem, Lord Of The Black",
                        Description="Narrow blazing eyes sit dreadfully within the creature's hard, thorny skull, which gives the creature a savage looking appearance."},
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Greghiesdonth, The Quiet",
                        Description="Tranquil ruby eyes sit narrowly within the creature's horned, thorny skull, which gives the creature a fierce looking appearance."},
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Ezesdum, The Barbarian",
                        Description="Wide emerald eyes sit delicately within the creature's horned, long skull, which gives the creature a fairly mellow looking appearance."},
                    new Item { Id = Guid.NewGuid().ToString(), Text = "Noavudu, The Firestarter",
                        Description="Wide cobalt eyes sit wickedly within the creature's long, narrow skull, which gives the creature a frightening looking appearance."},
                };

                foreach (Item item in items)
                {
                    StoreItem(item);
                }
            }


        }

        public async Task<bool> AddItemAsync(Item item)
        {
            StoreItem(item);

            return await Task.FromResult(true);
        }

        public IRealmCollection<Item> GetItems()
        {
            return _items;
        }

        private void StoreItem(Item item) {
            // Update and persist objects with a thread-safe transaction
            _realm.Write(() =>
            {
                _realm.Add(
                    new Item {
                        Id = _items.Count().ToString(),
                        Text = item.Text,
                        Description = item.Description
                    }
                );
            });
        }
    }
}
