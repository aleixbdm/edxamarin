﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Realms;

namespace edxamarin.Services
{
    public interface IItemsService<T>
    {
        Task<bool> AddItemAsync(T item);
        IRealmCollection<T> GetItems();
    }
}