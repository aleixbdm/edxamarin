﻿using System;
using System.Threading.Tasks;
using edxamarin.ViewModels;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;

namespace edxamarin
{
    public class ItemDetailViewModel : BaseViewModel<Item>
    {
        private readonly IMvxNavigationService _navigationService;

        public Item Item { get; set; }

        public ItemDetailViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Dragon Details";
        }

        public override void Prepare(Item parameter)
        {
            Item = parameter;
        }

    }
}
