﻿using edxamarin.Services;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;

namespace edxamarin.ViewModels
{
    public abstract class BaseViewModel : MvxViewModel
    {
        protected BaseViewModel()
        {
            DataStore = Mvx.Resolve<IItemsService<Item>>();
        }

        public readonly IItemsService<Item> DataStore;

        bool isBusy = false;
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        string title = string.Empty;
        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }

    }

    public abstract class BaseViewModel<TParameter> : MvxViewModel<TParameter>
        where TParameter : class
    {
        protected BaseViewModel()
        {
            DataStore = Mvx.Resolve<IItemsService<Item>>();
        }

        public readonly IItemsService<Item> DataStore;

        bool isBusy = false;
        public bool IsBusy
        {
            get => isBusy;
            set => SetProperty(ref isBusy, value);
        }

        string title = string.Empty;
        public string Title
        {
            get => title;
            set => SetProperty(ref title, value);
        }
    }

}
