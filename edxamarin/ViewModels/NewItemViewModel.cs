﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;

namespace edxamarin.ViewModels
{
    public class NewItemViewModel : BaseViewModel
    {
        public MainViewModel _mainViewModel;
        public IMvxCommand<Item> AddItemCommand { get; set; }

        public NewItemViewModel()
        {
            Title = "Add Dragon";

            AddItemCommand = new MvxAsyncCommand<Item>(AddItem);
        }

        private async Task AddItem(Item item)
        {
            await DataStore.AddItemAsync(item);
        }
    }
}
