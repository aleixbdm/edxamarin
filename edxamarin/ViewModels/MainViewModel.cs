﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using MvvmCross.Core.Navigation;
using MvvmCross.Core.ViewModels;
using Realms;

namespace edxamarin.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private readonly IMvxNavigationService _navigationService;

        public IRealmCollection<Item> Items { get; set; }

        public IMvxCommand ShowAddItemCommand { get; private set; }
        public IMvxCommand<Item> ItemSelectedCommand { get; private set; }

        public MainViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;

            Title = "Dragons";
            Items = DataStore.GetItems();

            ShowAddItemCommand = new MvxAsyncCommand(async () => await _navigationService.Navigate<NewItemViewModel>());
            ItemSelectedCommand = new MvxAsyncCommand<Item>(ItemSelected);
        }

        public override void Start()
        {
            base.Start();
        }

        private async Task ItemSelected(Item selectedItem)
        {
            await _navigationService.Navigate<ItemDetailViewModel, Item>(selectedItem);
        }

    }
}
