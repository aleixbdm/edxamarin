﻿using Foundation;
using UIKit;
using MvvmCross.iOS.Platform;
using MvvmCross.iOS.Views.Presenters;
using MvvmCross.Platform;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform.UI;

namespace edxamarin.iOS
{
    [Register("AppDelegate")]
    public class AppDelegate : MvxApplicationDelegate
    {
        public override UIWindow Window { get; set; }

        public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
        {
            Window = new UIWindow(UIScreen.MainScreen.Bounds);

            var presenter = new MvxIosViewPresenter(this, Window);

            var setup = new Setup(this, presenter);
            setup.Initialize();

            var startup = Mvx.Resolve<IMvxAppStart>();
            startup.Start();

            Window.MakeKeyAndVisible();

            CustomizeAppearance();

            return true;
        }

        private void CustomizeAppearance()
        {
            UINavigationBar.Appearance.SetTitleTextAttributes(new UITextAttributes()
            {
                TextColor = UIColor.White,
                Font = UIFont.SystemFontOfSize(17f, UIFontWeight.Semibold)
            });
            var NativeColorConv = Mvx.Resolve<IMvxNativeColor>();
            var PrimaryNativeColor = NativeColorConv.ToNative(AppColors.PrimaryColor) as UIColor;

            UINavigationBar.Appearance.Translucent = false;
            UINavigationBar.Appearance.BarTintColor = PrimaryNativeColor;
            UINavigationBar.Appearance.TintColor = UIColor.White;
            UINavigationBar.Appearance.BackgroundColor = PrimaryNativeColor;
            UINavigationBar.Appearance.BackIndicatorImage = new UIImage();
        }

    }
}
