﻿using System;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using UIKit;

namespace edxamarin.iOS.Views.Cells
{
    public partial class ItemCell : MvxTableViewCell
    {
        public static readonly UINib Nib = UINib.FromName("ItemCell", NSBundle.MainBundle);
        public static readonly NSString Key = new NSString("ItemCell");
        public static readonly int Height = 50;

        public ItemCell(IntPtr handle) : base(handle) 
        {
            InitialiseBindings();
        }

        private void InitialiseBindings()
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<ItemCell, Item>();
                set.Bind(Title).To(item => item.Text);
                set.Bind(Subtitle).To(item => item.Description);
                set.Apply();
            });
        }
    }
}
