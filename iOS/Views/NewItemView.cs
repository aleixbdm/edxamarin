﻿using System;
using edxamarin.ViewModels;
using MvvmCross.iOS.Views;
using MvvmCross.Platform;
using MvvmCross.Platform.UI;
using UIKit;

namespace edxamarin.iOS.Views
{
    public partial class NewItemView : MvxViewController
    {
        public NewItemViewModel NewItemViewModel { get; set; }

        public NewItemView()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NewItemViewModel = ViewModel as NewItemViewModel;

            Title = NewItemViewModel.Title;

            var NativeColorConv = Mvx.Resolve<IMvxNativeColor>();
            var PrimaryNativeColor = NativeColorConv.ToNative(AppColors.PrimaryColor) as UIColor;
            btnSaveDragon.TintColor = PrimaryNativeColor;

            btnSaveDragon.TouchUpInside += (sender, e) =>
            {
                var item = new Item
                {
                    Text = txtName.Text,
                    Description = txtDesc.Text
                };
                NewItemViewModel.AddItemCommand.Execute(item);
                NavigationController.PopToRootViewController(true);
            };
        }

    }
}

