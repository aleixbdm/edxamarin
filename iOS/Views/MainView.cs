﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using edxamarin;
using edxamarin.iOS.Views.Cells;
using edxamarin.ViewModels;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using UIKit;

namespace edxamarin.iOS
{
    public class MainView : MvxTableViewController
    {

        UIRefreshControl refreshControl;
        private UIBarButtonItem _btnAddItem;

        public MainViewModel MainViewModel { get; set; }

        public MainView()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            MainViewModel = ViewModel as MainViewModel;

            Title = MainViewModel.Title;
            _btnAddItem = new UIBarButtonItem(UIBarButtonSystemItem.Add, (sender, args) =>
            {
                // button was clicked
                MainViewModel.ShowAddItemCommand.Execute(null);
            });
            NavigationItem.SetRightBarButtonItem(_btnAddItem, false);

            // Setup UITableView.
            refreshControl = new UIRefreshControl();
            refreshControl.ValueChanged += RefreshControl_ValueChanged;
            TableView.Add(refreshControl);

            var source = new TableSource(TableView);
            var set = this.CreateBindingSet<MainView, MainViewModel>();
            set.Bind(source).To(MainViewModel => MainViewModel.Items);
            set.Bind(source).For(v => v.SelectionChangedCommand).To(vm => vm.ItemSelectedCommand);
            set.Apply();

            TableView.Source = source;

            TableView.RowHeight = ItemCell.Height;
            TableView.ReloadData();

            MainViewModel.PropertyChanged += IsBusy_PropertyChanged;
            MainViewModel.Items.CollectionChanged += Items_CollectionChanged;
        }

        void RefreshControl_ValueChanged(object sender, EventArgs e)
        {
            InvokeOnMainThread(() => TableView.ReloadData());
            if (!MainViewModel.IsBusy && refreshControl.Refreshing)
            {
                MainViewModel.IsBusy = true;
                MainViewModel.IsBusy = false;
            }
        }

        void IsBusy_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var propertyName = e.PropertyName;
            switch (propertyName)
            {
                case nameof(MainViewModel.IsBusy):
                    {
                        InvokeOnMainThread(() =>
                        {
                            if (MainViewModel.IsBusy && !refreshControl.Refreshing)
                                refreshControl.BeginRefreshing();
                            else if (!MainViewModel.IsBusy)
                                refreshControl.EndRefreshing();
                        });
                    }
                    break;
            }
        }

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            InvokeOnMainThread(() => TableView.ReloadData());
        }

        public class TableSource : MvxTableViewSource
        {
            private static readonly NSString ItemCellIdentifier = new NSString("ItemCell");

            public TableSource(UITableView tableView) : base(tableView)
            {
                tableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;
                tableView.RegisterNibForCellReuse(UINib.FromName("ItemCell", NSBundle.MainBundle), ItemCellIdentifier);
            }

            protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
            {
                NSString cellIdentifier;
                if (item is Item)
                {
                    cellIdentifier = ItemCellIdentifier;
                }
                else
                {
                    throw new ArgumentException("Unknown item of type " + item.GetType().Name);
                }

                var tableViewCell = (ItemCell)TableView.DequeueReusableCell("ItemCell");
                return tableViewCell;
            }
        }
    }
}