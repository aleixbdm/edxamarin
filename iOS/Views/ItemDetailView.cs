﻿using System;
using edxamarin.ViewModels;
using MvvmCross.iOS.Views;
using UIKit;

namespace edxamarin.iOS.Views
{
    public partial class ItemDetailView : MvxViewController
    {
        public ItemDetailViewModel ItemDetailViewModel { get; set; }

        public ItemDetailView()
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            ItemDetailViewModel = ViewModel as ItemDetailViewModel;

            Title = ItemDetailViewModel.Title;
            ItemNameLabel.Text = ItemDetailViewModel.Item.Text;
            ItemDescriptionLabel.Text = ItemDetailViewModel.Item.Description;

        }

    }
}

